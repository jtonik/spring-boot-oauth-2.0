/*  PROPRIETARY/CONFIDENTIAL
 *  This software has been provided pursuant to a License Agreement containing
 *  restrictions on its use. This source code is Copyright (c) 2009-2014 and is the 
 *  exclusive property of Imagine Software Inc., 233 Broadway, 17th Floor, New York, 
 *  NY 10279. All rights reserved.  None of the content of any of this source
 *  code may be reproduced, transcribed, stored in a retrieval system,
 *  translated into any other language or other computer language, or
 *  transmitted in any form or by any means (electrical, mechanical,
 *  photocopied, recorded or otherwise) without prior written permission
 *  of Imagine Software. This software contains valuable trade secrets and
 *  proprietary information of Imagine Software Inc., and is protected by
 *  federal, state, and local laws.
 */
package com.cleverua.oauth2.client;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Configuration
@EnableAutoConfiguration
@EnableOAuth2Client
@RestController
public class ClientApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(ClientApplication.class)
                .properties("spring.config.name=client")
                .build().run(args);
//        SpringApplication.run(ClientApplication.class, args);
    }

    @Value("${oauth.resource:http://localhost:8080}")
    private String baseUrl;

    @Value("${oauth.authorize:http://localhost:8080/oauth/authorize}")
    private String authorizeUrl;

    @Value("${oauth.token:http://localhost:8080/oauth/token}")
    private String tokenUrl;

    @Autowired
    private OAuth2RestOperations restTemplate;

    @RequestMapping("/")
    public Map<String, String> home() {
        @SuppressWarnings("unchecked")
        Map<String, String> result = restTemplate.getForObject(baseUrl + "/hello", Map.class);
        return result;
    }

    @Bean
    public OAuth2RestOperations restTemplate(OAuth2ClientContext oauth2ClientContext) {
        return new OAuth2RestTemplate(resource(), oauth2ClientContext);
    }

    @Bean
    protected OAuth2ProtectedResourceDetails resource() {
        AuthorizationCodeResourceDetails resource = new AuthorizationCodeResourceDetails();
        resource.setAccessTokenUri(tokenUrl);
        resource.setUserAuthorizationUri(authorizeUrl);
        resource.setClientId("client");
        resource.setClientSecret("secret");
        return resource ;
    }

}