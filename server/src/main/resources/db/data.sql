INSERT INTO users (username, password, enabled) VALUES ('user', 'user', TRUE);
INSERT INTO users (username, password, enabled) VALUES ('admin', 'admin',  TRUE);

INSERT INTO authorities (username, authority) VALUES ('user', 'ROLE_CLIENT');
INSERT INTO authorities (username, authority) VALUES ('admin',  'ROLE_CLIENT');
INSERT INTO authorities (username, authority) VALUES ('admin',  'ROLE_TRUSTED_CLIENT');