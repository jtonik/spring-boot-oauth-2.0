/*  PROPRIETARY/CONFIDENTIAL
 *  This software has been provided pursuant to a License Agreement containing
 *  restrictions on its use. This source code is Copyright (c) 2009-2014 and is the 
 *  exclusive property of Imagine Software Inc., 233 Broadway, 17th Floor, New York, 
 *  NY 10279. All rights reserved.  None of the content of any of this source
 *  code may be reproduced, transcribed, stored in a retrieval system,
 *  translated into any other language or other computer language, or
 *  transmitted in any form or by any means (electrical, mechanical,
 *  photocopied, recorded or otherwise) without prior written permission
 *  of Imagine Software. This software contains valuable trade secrets and
 *  proprietary information of Imagine Software Inc., and is protected by
 *  federal, state, and local laws.
 */
package com.cleverua.oauth2.server.config;

import org.h2.server.web.WebServlet;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

/**
 * TODO javadoc
 */
@Configuration
public class H2Configuration
{
    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .setName("auth")
                .addScript("db/schema.sql")
                .addScript("db/data.sql")
                .build();
    }

    @Bean
    public ServletRegistrationBean h2ServletRegistrationBean() {
        WebServlet webServlet = new WebServlet();
        ServletRegistrationBean bean = new ServletRegistrationBean(webServlet);
        bean.addUrlMappings("/h2console/*");
        return bean;
    }

}
