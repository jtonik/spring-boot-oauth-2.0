/*  PROPRIETARY/CONFIDENTIAL
 *  This software has been provided pursuant to a License Agreement containing
 *  restrictions on its use. This source code is Copyright (c) 2009-2014 and is the 
 *  exclusive property of Imagine Software Inc., 233 Broadway, 17th Floor, New York, 
 *  NY 10279. All rights reserved.  None of the content of any of this source
 *  code may be reproduced, transcribed, stored in a retrieval system,
 *  translated into any other language or other computer language, or
 *  transmitted in any form or by any means (electrical, mechanical,
 *  photocopied, recorded or otherwise) without prior written permission
 *  of Imagine Software. This software contains valuable trade secrets and
 *  proprietary information of Imagine Software Inc., and is protected by
 *  federal, state, and local laws.
 */
package com.cleverua.oauth2.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;

/**
 * TODO javadoc
 */
@SpringBootApplication
public class ServerApplication
{
    private static final String CLIENT_ID = "client";
    private static final String RESOURCE_ID = "oauth2-resource";

    public static void main(String[] args)
    {
        new SpringApplicationBuilder(ServerApplication.class)
                .properties("spring.config.name=server")
                .build().run(args);
    }

    @Autowired
    private DataSource dataSource;

    @Bean
    public TokenStore tokenStore() {
//            return new InMemoryTokenStore();
        return new JdbcTokenStore(dataSource);
    }

    @Bean
    public WebMvcConfigurerAdapter webMvcConfigurerAdapter() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                registry.addViewController("/").setViewName("forward:/index.html");
            }
        };
    }

    @Configuration
    @EnableAuthorizationServer
    protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

        @Autowired
        private TokenStore tokenStore;

        @Autowired
        @Qualifier("authenticationManagerBean")
        private AuthenticationManager authenticationManager;

        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            clients.inMemory()
                    .withClient(CLIENT_ID)
                    .secret("secret")
                    .authorizedGrantTypes("password", "authorization_code", "refresh_token", "implicit")
                    .authorities("ROLE_CLIENT", "ROLE_TRUSTED_CLIENT")
                    .scopes("read", "write", "trust")
                    .resourceIds(RESOURCE_ID)
                    .accessTokenValiditySeconds(600)
                    .autoApprove(true);
        }

        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
            endpoints
                    .tokenStore(tokenStore)
                    .authenticationManager(authenticationManager);
        }

        @Override
        public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
            oauthServer
                    .tokenKeyAccess("isAnonymous() || hasRole('TRUSTED_CLIENT')") // permitAll()
                    .checkTokenAccess("hasRole('TRUSTED_CLIENT')"); // isAuthenticated()
        }

    }

    @Configuration
    @EnableResourceServer
    @RestController
    protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

        private static final String RESOURCE_URL = "/hello";

        @Autowired
        private TokenStore tokenStore;

        @RequestMapping(value = RESOURCE_URL)
        public Map<String, String> hello() {
            return Collections.singletonMap("greeting", "Hello World! " + UUID.randomUUID().toString());
        }


        @Override
        public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
            resources
                    .tokenStore(tokenStore)
                    .resourceId(RESOURCE_ID).stateless(true);
        }

        @Override
        public void configure(HttpSecurity http) throws Exception {
            http.antMatcher(RESOURCE_URL)
                    .authorizeRequests().anyRequest().authenticated();
        }
    }
}
