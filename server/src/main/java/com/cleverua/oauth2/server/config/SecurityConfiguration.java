/*  PROPRIETARY/CONFIDENTIAL
 *  This software has been provided pursuant to a License Agreement containing
 *  restrictions on its use. This source code is Copyright (c) 2009-2014 and is the 
 *  exclusive property of Imagine Software Inc., 233 Broadway, 17th Floor, New York, 
 *  NY 10279. All rights reserved.  None of the content of any of this source
 *  code may be reproduced, transcribed, stored in a retrieval system,
 *  translated into any other language or other computer language, or
 *  transmitted in any form or by any means (electrical, mechanical,
 *  photocopied, recorded or otherwise) without prior written permission
 *  of Imagine Software. This software contains valuable trade secrets and
 *  proprietary information of Imagine Software Inc., and is protected by
 *  federal, state, and local laws.
 */
package com.cleverua.oauth2.server.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dateSource;

    @Autowired
    public void globalUserDetails(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dateSource);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        //TODO AD remove webjars
        web.ignoring().antMatchers(
                "/h2console/**",
                "/oauth/uncache_approvals",
                "/oauth/cache_approvals");
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/login", "/").permitAll()
                    .anyRequest().hasRole("CLIENT")
//                .and().exceptionHandling()
//                    .accessDeniedPage("/login.jsp?authorization_error=true")
//                .and().csrf()
//                    .requireCsrfProtectionMatcher(new AntPathRequestMatcher("/oauth/authorize")).disable()
                .and().httpBasic();
    }

}
